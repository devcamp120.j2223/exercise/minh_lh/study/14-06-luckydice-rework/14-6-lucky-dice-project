// Khai báo mongoose
const mongoose = require('mongoose');

//Import controller
const voucherModel = require('../model/voucherModel');


// CREATE  A VOUCHER
const createVoucher = (request, response) => {
    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!bodyRequest.code) {
        return response.status(400).json({
            status: " Error 400: Bad Request",
            message: "Code is require"
        })
    }

    if (!bodyRequest.node) {
        return response.status(400).json({
            status: " Error 400: Bad Request",
            message: "Node  is require"
        })
    }

    if (!(Number.isInteger(bodyRequest.discount) && bodyRequest.discount > 0)) {
        return response.status(400).json({
            status: " Error 400: Bad Request",
            message: "Discount is not valid"
        })
    }

    // B3: thao tắc với cơ sở dữ liệu
    let createVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: bodyRequest.code,
        discount: bodyRequest.discount,
        node: bodyRequest.node
    }
    voucherModel.create(createVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create voucher success",
                data: data
            })
        }
    })
}


//GET ALL VOUCHER
const getAllVoucher = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    // B3: thao tắc với cơ sở dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all voucher success",
                data: data
            })
        }
    })
}


//  GET A VOUCHER BY ID
const getVoucherById = (request, response) => {
    //B1: Thu thập dữ liệu
    let voucherId = request.params.voucherId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Voucher ID is not valid"
        })
    }

    //B3: thao tắc với cơ sở dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get voucher bi ID success",
                data: data
            })
        }
    })
}


// UPDATE BY ID
const updateVoucher = (request, response) => {
    //B1: thu thập dữ liệu
    let voucherId = request.params.voucherId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: " Error 400: Bad Request",
            message: "Voucher ID is invalid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let updateVoucher = {
        code: bodyRequest.code,
        discount: bodyRequest.discount,
        node: bodyRequest.node
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Server Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Voucher By ID success",
                data: data
            })
        }
    })
}


// DELETE A VOUCHER
const deleteVoucherById = (request, response) => {
    //B1: thu thập dữ liệu
    let voucherId = request.params.voucherId;

    //B2: validate dữ liệu

    //B3: Thao tác với cơ sở dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal Sever",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Delete Voucher By Id successfully",
            })
        }
    })
}

//EXPORT
module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucher, deleteVoucherById }