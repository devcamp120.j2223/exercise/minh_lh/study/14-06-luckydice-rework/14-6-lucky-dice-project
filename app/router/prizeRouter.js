// Khai báo thư viện express
const express = require('express');

//Import Middleware
const { prizeMiddleware } = require('../middleware/prizeMiddleware');


//Import  Controller
const { createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById } = require('../controller/prizeController');

//Tạo router
const prizeRouter = express.Router();

//Sử dụng middleware
prizeRouter.use(prizeMiddleware);

// KHAI BÁO API

//CREATE A Prize
prizeRouter.post('/prizes', createPrize);


//GET ALL Prize
prizeRouter.get('/prizes', getAllPrize);


//GET A Prize
prizeRouter.get('/prizes/:prizeId', getPrizeById);


//UPDATE A Prize
prizeRouter.put('/prizes/:prizeId', updatePrizeById);


//DELETE A Prize
prizeRouter.delete('/prizes/:prizeId', deletePrizeById);

// EXPORT ROUTER
module.exports = prizeRouter;


// {
//     "name": "Hoàng",
//     "description": "Ê tô"
// }